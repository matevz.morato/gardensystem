#ifndef TCP_SOCKET_H
#define TCP_SOCKET

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/event_groups.h>
#include <sys/param.h>
#include <sys/time.h>
#include <driver/gpio.h>
#include <esp_log.h>
#include <nvs_flash.h>

#include <esp_wifi.h>
#include <esp_system.h>
#include <esp_event.h>
#include <esp_netif.h>

#include <lwip/err.h>
#include <lwip/sys.h>
#include <lwip/netdb.h>
#include <lwip/sockets.h>


#define PORT                        3333
#define KEEPALIVE_IDLE              5
#define KEEPALIVE_INTERVAL          5
#define KEEPALIVE_COUNT             3

void tcp_server_task(void *pvParameters);
extern uint32_t relay_front_start_t;
extern uint32_t relay_back_start_t;
extern int relay_front_state;
extern int relay_back_state;


#endif