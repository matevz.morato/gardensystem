#ifndef CUSTOM_WIFI_DRIVER_H
#define CUSTOM_WIFI_DRIVER_H

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/event_groups.h>
#include <sys/param.h>
#include <sys/time.h>
#include <driver/gpio.h>
#include <esp_log.h>
#include <nvs_flash.h>

#include <esp_wifi.h>
#include <esp_system.h>
#include <esp_event.h>
#include <esp_netif.h>

#include <lwip/err.h>
#include <lwip/sys.h>
#include <lwip/netdb.h>
#include <lwip/sockets.h>

#define EXAMPLE_ESP_WIFI_SSID      "Free wifi!"
#define EXAMPLE_ESP_WIFI_PASS      "041828688"
#define EXAMPLE_ESP_MAXIMUM_RETRY  10

#define WIFI_CONNECTED_BIT BIT0
#define WIFI_FAIL_BIT      BIT1

void wifi_init_sta(void);





#endif