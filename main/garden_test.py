import os
import re
import socket
import sys
import time


# -----------  Config  ----------
PORT = 3333
ADDRESS = "192.168.64.110"
# -------------------------------


def tcp_client(address, payload):
    for res in socket.getaddrinfo(address, PORT, socket.AF_UNSPEC,
                                  socket.SOCK_STREAM, 0, socket.AI_PASSIVE):
        family_addr, socktype, proto, canonname, addr = res
    try:
        sock = socket.socket(family_addr, socket.SOCK_STREAM)
        sock.settimeout(60.0)
    except socket.error as msg:
        print('Could not create socket: ' + str(msg[0]) + ': ' + msg[1])
        raise
    try:
        sock.connect(addr)
    except socket.error as msg:
        print('Could not open socket: ', msg)
        sock.close()
        raise
    sock.sendall(payload.encode())
    data = sock.recv(1024)
    if not data:
        return
    print('Reply : ' + data.decode())
    sock.close()
    return data.decode()

def start_front():
    tcp_client(ADDRESS, "start_front")


def stop_front():
    tcp_client(ADDRESS, "stop_front")


def start_back():
    tcp_client(ADDRESS, "start_back")


def stop_back():
    tcp_client(ADDRESS, "stop_back")




while(not (time.localtime().tm_hour == 5 and time.localtime().tm_min >= 5)):
    time.sleep(120)
    print(time.localtime())
    print("Spim!")



for i in range(10):
    print(f"Zacenjamo {i} cikel")
    start_front()
    time.sleep(6*60)
    stop_front()
    time.sleep(60)
    start_back()
    time.sleep(8.5*60)
    stop_back()
    time.sleep(1*60)

